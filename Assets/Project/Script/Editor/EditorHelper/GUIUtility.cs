﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EditorHelper {
    public class GUIUtility {
        
        /// <summary>
        /// ルーラー
        /// </summary>
        public static void Rule () {
            GUILayout.Space(10);
            GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(1));
            GUILayout.Space(10);
        }

        /// <summary>
        /// タブ
        /// </summary>
        public static int Tab (string[] tabs, int selected) {
			var skin = (GUISkin)Resources.Load("FlatTab");
			GUIStyle tabStyle = skin.GetStyle("Tab");
			var defaultSkin = GUI.skin;
			GUI.skin = skin;
			selected = GUILayout.Toolbar(selected, tabs, tabStyle);
			GUI.skin = defaultSkin;

            return selected;
		}
    }
}
