﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ParticleTimeline {
	public class TimeControl {
		public bool isPlaying { get; private set; }

		public float minTime { get; private set; }

		public float maxTime { get; private set; }

		public float currentTime {
			get {
				m_currentTime = Mathf.Repeat (m_currentTime, maxTime);
				m_currentTime = Mathf.Clamp (m_currentTime, minTime, maxTime);
				return m_currentTime;
			}
			set {
				m_currentTime = value;
			}
		}
		private double m_lastFrameEditorTime = 0;
    	private float m_currentTime;

		 public TimeControl () {
			EditorApplication.update += TimeUpdate;
		}

		public void SetMinMaxTime (float minTime, float maxTime) {
			this.minTime = minTime;
			this.maxTime = maxTime;
		}

		/// <summary>
		/// 時間更新
		/// </summary>
		private void TimeUpdate () {
			if ( isPlaying ) {
				var timeSinceStartup = EditorApplication.timeSinceStartup;
				var deltaTime = timeSinceStartup - m_lastFrameEditorTime;
				m_lastFrameEditorTime = timeSinceStartup;
				m_currentTime += (float)deltaTime;
			}
		}

		/// <summary>
		/// 再生
		/// </summary>
		public void Play () {
			// Debug.Log("再生");
			isPlaying = true;
			m_lastFrameEditorTime = EditorApplication.timeSinceStartup;
		}

		/// <summary>
		/// 一時停止
		/// </summary>
		public void Pause () {
			// Debug.Log("一時停止");
			isPlaying = false;
		}

		/// <summary>
		/// 停止
		/// </summary>
		public void Stop () {
			// Debug.Log("停止");
			isPlaying = false;
			currentTime = 0;
		}
	}
}
