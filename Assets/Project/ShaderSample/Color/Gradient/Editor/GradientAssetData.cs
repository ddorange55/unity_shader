﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CustomShaderGUI {
	public class GradientAssetData : ScriptableObject {
		public Gradient gradient;
		public Texture2D texture;
	}
}