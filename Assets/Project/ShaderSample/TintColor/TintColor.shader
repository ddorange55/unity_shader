﻿Shader "Custom/TintColor" {

	Properties {
		
		_MainTex("Main Tex", 2D) = "white" {}
		_TintColor ("Tint Color", Color) = (0.5,0.5,0.5,1.0)
	}

	Category {
		Tags {
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}
		
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off

		SubShader {
			Pass {
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _MainTex_ST;
				float4 _TintColor;

				struct appdata_t {
					float4 vertex : POSITION;
					float2 texcoord: TEXCOORD0;
				};

				struct v2f {
					float4 vertex : POSITION;
					float2 uv     : TEXCOORD0;
				};


				v2f vert (appdata_t v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv  = TRANSFORM_TEX (v.texcoord, _MainTex);
					return o;
				}

				fixed4 frag( v2f i ) : COLOR
				{
					fixed4 col = tex2D(_MainTex, i.uv);

					// 頂点カラーの設定
					col.rgb += (_TintColor.rgb - float3(0.5, 0.5, 0.5)) * 2;
					col.a *= _TintColor.a;
					
					return col;
				}
				ENDCG
			}
		}
	}
}