﻿Shader "Custom/UV/UVScroll_01"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_ScrollSpeedX("UV Scroll Speed X", Float) = 0
		_ScrollSpeedY("UV Scroll Speed Y", Float) = 0
	}
	SubShader
	{
		Tags
		{
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
		}
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode Off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile_particles
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _ScrollSpeedX;
			float _ScrollSpeedY;

			struct appdata_t {
				float4 vertex   : POSITION;
				fixed4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 uv     : TEXCOORD0;
				fixed4 color  : COLOR;
			};
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);

				// UVスクロール
				// 実機でのバグ検証のためUV値が0~1の間で変化するようにしている
				o.uv.x += lerp(0, 1, _Time * _ScrollSpeedX);
				o.uv.y += lerp(0, 1, _Time * _ScrollSpeedY);

				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				col *= i.color;
				return col;
			}
			ENDCG
		}
	}
}
