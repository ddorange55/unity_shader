﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ParticleTimeline {
	public class Track {

		public int id;
		
		public string name;
		
		public float dur = 0.0f;
		
		public float delay = 0.0f;
		
		public bool active = true;
		
		public int depth = 0;
	}
}
