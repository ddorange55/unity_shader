﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace CustomShaderGUI {
	public class GeneralShaderGUIBase : ShaderGUI {

		protected MaterialEditor materialEditor;
		protected Material material;

		// ブレンドモード
		protected enum BLEND_MODE {
			ALPHA_BLEND = 0,
			ADDITIVE,
			MULTIPLY,
			SUBTRACTIVE
		}

		protected enum Z_TEST {
			LESS_EQUAL = 0,
			ALWAYS
		}

		protected MaterialProperty mainTex = null;
		protected MaterialProperty blendMode = null;
		protected MaterialProperty SrcBlendMode = null;
		protected MaterialProperty DstBlendMode = null;
		protected MaterialProperty tintColor = null;
		protected MaterialProperty cull = null;
		protected MaterialProperty zwrite = null;
		protected MaterialProperty ztest = null;

		protected bool toggleZControlOpen = false;
		protected Z_TEST zTestMode = Z_TEST.LESS_EQUAL;

		public override void OnGUI(MaterialEditor editor, MaterialProperty[] props) {
			materialEditor = editor;
			material = editor.target as Material;
			FindProperties(props);
			GetZTestMode(material);
			ShaderPropertiesGUI(editor, material);
		}

		/// <summary>
        /// Shader切り替えコールバック
        /// MaterialのShader切り替え時にBlend指定が変更されてしまうので、再設定する必要がある
        /// </summary>
        public override void AssignNewShaderToMaterial(Material mat, Shader old, Shader news) {
            base.AssignNewShaderToMaterial(mat, old, news);
            SetBlendMode(mat, (BLEND_MODE)mat.GetFloat("_BlendMode"));
        }

		/// <summary>
		/// Shaderに設定されているプロパティを取得する
		/// </summary>
		public virtual void FindProperties(MaterialProperty[] props) {
			mainTex        = FindProperty("_MainTex",        props);
			blendMode      = FindProperty("_BlendMode",      props);
			SrcBlendMode   = FindProperty("_SrcBlendMode",   props);
			DstBlendMode   = FindProperty("_DstBlendMode",   props);
			tintColor      = FindProperty("_TintColor",      props);
			cull           = FindProperty("_CullMode",       props);
        	zwrite         = FindProperty("_ZWriteMode",     props);
        	ztest          = FindProperty("_DepthTestMode",  props);
		}

		/// <summary>
		/// ShaderのプロパティをGUIに表示する
		/// </summary>
		public virtual void ShaderPropertiesGUI(MaterialEditor editor, Material mat) {
			EditorGUIUtility.labelWidth = 0f;
			MainSettingArea(mat, editor);
			editor.RenderQueueField();
		}

		/// <summary>
		/// 基本設定の描画
		/// </summary>
		protected void MainSettingArea (Material mat, MaterialEditor editor) {
			
			EditorGUILayout.BeginVertical(GUI.skin.box);
			{
				EditorGUILayout.LabelField("Main Control", EditorStyles.boldLabel);
				editor.TextureProperty(mainTex, "Texture (RGBA)");
				editor.ColorProperty(tintColor, "Tint Color");

				// ブレンドモード
				BlendModePopup(mat);

				// デプス制御
				EditorGUILayout.Space();
				EditorGUI.indentLevel++;
				toggleZControlOpen = EditorGUILayout.Foldout(toggleZControlOpen, "Z Control");
				// EditorGUILayout.LabelField("Z Control", EditorStyles.boldLabel);
				if (toggleZControlOpen) {
					editor.ShaderProperty(cull,   "Culling");
					editor.ShaderProperty(zwrite, "Z Write");

					// Z Test は選択肢を絞る
					EditorGUI.BeginChangeCheck();
					zTestMode =(Z_TEST)EditorGUILayout.EnumPopup("Z Test", zTestMode);
					if (EditorGUI.EndChangeCheck()) {
						SetZTestMode(mat, zTestMode);
					}
				}
				EditorGUI.indentLevel--;
			}
			EditorGUILayout.EndVertical();
		}

		protected void GetZTestMode (Material mat) {
			if (mat.GetFloat("_DepthTestMode") == (float)UnityEngine.Rendering.CompareFunction.Always) {
				zTestMode = Z_TEST.ALWAYS;
			} else {
				zTestMode = Z_TEST.LESS_EQUAL;
			}
		}

		protected void SetZTestMode (Material material, Z_TEST test) {
			if (test == Z_TEST.ALWAYS) {
				material.SetFloat("_DepthTestMode", (float)UnityEngine.Rendering.CompareFunction.Always);
			} else {
				material.SetFloat("_DepthTestMode", (float)UnityEngine.Rendering.CompareFunction.LessEqual);
			}
		}

		/// <summary>
		/// ブレンドモードのセレクトを表示する
		/// </summary>
		protected void BlendModePopup (Material mat) {
			var mode = (BLEND_MODE)blendMode.floatValue;
			
			EditorGUI.BeginChangeCheck();
			mode = (BLEND_MODE)EditorGUILayout.EnumPopup("Blend Mode", mode);
			if (EditorGUI.EndChangeCheck()) {
				mat.SetFloat("_BlendMode", (float)mode);
				SetBlendMode(mat, mode);
			}
		}

		/// <summary>
		/// ブレンドモード切り替え
		/// </summary>
		protected void SetBlendMode (Material mat, BLEND_MODE mode) {
			switch(mode) {
				case BLEND_MODE.ALPHA_BLEND:
					mat.SetFloat("_SrcBlendMode", (float)UnityEngine.Rendering.BlendMode.SrcAlpha);
					mat.SetFloat("_DstBlendMode", (float)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
					break;
				case BLEND_MODE.ADDITIVE:
					mat.SetFloat("_SrcBlendMode", (float)UnityEngine.Rendering.BlendMode.SrcAlpha);
					mat.SetFloat("_DstBlendMode", (float)UnityEngine.Rendering.BlendMode.One);
					break;
				case BLEND_MODE.MULTIPLY:
					mat.SetFloat("_SrcBlendMode", (float)UnityEngine.Rendering.BlendMode.DstColor);
					mat.SetFloat("_DstBlendMode", (float)UnityEngine.Rendering.BlendMode.Zero);
					break;
				case BLEND_MODE.SUBTRACTIVE:
					mat.SetFloat("_SrcBlendMode", (float)UnityEngine.Rendering.BlendMode.Zero);
					mat.SetFloat("_DstBlendMode", (float)UnityEngine.Rendering.BlendMode.OneMinusSrcColor);
					break;
			}
		}

        protected void SetKeyword(Material mat, string keyword, bool state) {
            if (state)
                mat.EnableKeyword(keyword);
            else
                mat.DisableKeyword(keyword);
        }
	}
}
