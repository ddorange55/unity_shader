﻿using UnityEngine;
using UnityEditor;

namespace CustomShaderGUI {
    public class GeneralDissolveShaderGUI : GeneralShaderGUIBase {

        protected MaterialProperty dissolveTex = null;
        protected MaterialProperty dissolveVal = null;
        protected MaterialProperty dissolveGlowColor = null;
        protected MaterialProperty dissolveInEdgeColor = null;
        protected MaterialProperty dissolveOutEdgeColor = null;


        public override void OnGUI(MaterialEditor editor, MaterialProperty[] props) {
            Material mat = editor.target as Material;
            FindProperties(props);
            ShaderPropertiesGUI(editor, mat);
        }

        public override void FindProperties(MaterialProperty[] props) {
            base.FindProperties(props);
            dissolveTex    = FindProperty("_DissolveTex",    props);
            dissolveVal    = FindProperty("_DissolveValue",  props);
            dissolveGlowColor    = FindProperty("_DissolveGlowColor",  props);
            dissolveInEdgeColor  = FindProperty("_DissolveInEdgeColor",  props);
            dissolveOutEdgeColor = FindProperty("_DissolveOutEdgeColor",  props);
        }

        public override void ShaderPropertiesGUI(MaterialEditor editor, Material mat) {
            EditorGUIUtility.labelWidth = 0f;

            MainSettingArea(mat, editor);

            // ディゾルブ制御のUI
            EditorGUILayout.BeginVertical(GUI.skin.box);
            {
                EditorGUILayout.LabelField("Dissolve", EditorStyles.boldLabel);
                editor.ShaderProperty(dissolveTex, "Texture (R)");
                editor.ShaderProperty(dissolveVal, "Dissolve Value");

                EditorGUILayout.Space();

                editor.ColorProperty(dissolveGlowColor,    "Glow Color");
                editor.ColorProperty(dissolveInEdgeColor,  "In Edge Color");
                editor.ColorProperty(dissolveOutEdgeColor, "Out Edge Color");
            }
            EditorGUILayout.EndVertical();

            editor.RenderQueueField();
        }
    }
}