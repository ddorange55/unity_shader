﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace CustomShaderGUI {
	public class GradientShaderGUI : ShaderGUI {
		Material material;
		bool first = true;
		const int TextureWidth = 256;
		const int TextureHight = 1;
		
		MaterialProperty mainTex = null;
		MaterialProperty gradientTex = null;
		GradientAssetData data;
		SerializedObject serializedGradientData;
		SerializedProperty gradientProperty;
		Texture2D tmpTex;


        public override void AssignNewShaderToMaterial(Material mat, Shader old, Shader news) {
			material = mat;
            base.AssignNewShaderToMaterial(mat, old, news);
			SetGradientTexture(mat);
        }

		public override void OnGUI(MaterialEditor editor, MaterialProperty[] props) {
			material = editor.target as Material;

			FindProperties(props);

			// OnEnableのフックがないので、初回のOnGUIの更新で初期化
			if (first) {
				first = false;
				SetGradientTexture(material);
			}

			ShaderPropertiesGUI(editor, material);
		}

		public virtual void FindProperties(MaterialProperty[] props) {
			mainTex     = FindProperty("_MainTex",          props);
			gradientTex = FindProperty("_GradientTex",      props);
		}

		public virtual void ShaderPropertiesGUI(MaterialEditor editor, Material mat) {
			EditorGUIUtility.labelWidth = 0f;

			// メインテクスチャ
			editor.TextureProperty(mainTex, "Texture (RGBA)");

			// ※ グラデーションテクスチャ
			editor.TextureProperty(gradientTex, "Gradient Tex(RGB)");

			// グラデーション設定
			serializedGradientData.Update();
			EditorGUI.BeginChangeCheck();

			EditorGUILayout.PropertyField(gradientProperty);

			if (EditorGUI.EndChangeCheck()) {
				serializedGradientData.ApplyModifiedProperties();
				SaveAssetData();
			}

			if (GUILayout.Button("Save")) {
				Gradient2Texture(data.gradient, ref data.texture);
				gradientTex.textureValue = tmpTex;
				mat.SetTexture("_GradientTex", tmpTex);
			}
		}

		void SetGradientTexture(Material mat) {
			// Dataをロードする
			var path =  GetGradientDataPath();
			data = AssetDatabase.LoadAssetAtPath<GradientAssetData>(path);

			if (tmpTex == null) {
				tmpTex = CreateTemporaryTexture();
			}

			// dataが存在しなければ作成する
			if (data == null) {
				Debug.Log("CreateAssetData");
				CreateAssetData (path);
			}

			// テクスチャを設定する
			serializedGradientData = new SerializedObject(data);
			gradientProperty = serializedGradientData.FindProperty("gradient");
			Gradient2Texture(data.gradient, ref tmpTex);
		}

		/// <summary>
        /// Assetsデータを生成する
        /// </summary>
		void CreateAssetData (string path) {
			data = ScriptableObject.CreateInstance<GradientAssetData>();
			data.gradient = new Gradient();

			// 保存先ディレクトリを作成
			string directoryPath = GetGradientDataDirectory();
			if (!Directory.Exists(directoryPath)) {
				Directory.CreateDirectory(directoryPath);
			}

			// テクスチャを作成する
			string texturePath = GetGradientTexturePath();
			File.WriteAllBytes(texturePath, tmpTex.EncodeToPNG());
			AssetDatabase.Refresh();

			// 読み込み可能にするため、textureの設定を変更する 
			TextureImporter texImporter = AssetImporter.GetAtPath(texturePath) as TextureImporter;
			if (texImporter != null) {
				texImporter.wrapMode = TextureWrapMode.Clamp;
				texImporter.isReadable = true;
				texImporter.mipmapEnabled = false;
			}

			// 作成したテクスチャを読み込む
			AssetDatabase.ImportAsset(texturePath, ImportAssetOptions.ForceUpdate);
			data.texture = AssetDatabase.LoadAssetAtPath<Texture2D>(texturePath);

			// アセットを作成して保存
			AssetDatabase.CreateAsset(data, path);
			SaveAssetData();
		}

		void SaveAssetData () {
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		// グラデーションエディタの値をテクスチャに保存する
		void Gradient2Texture (Gradient gradient, ref Texture2D tex) {
			for (int w = 0; w < tex.width; ++w) {
				float rate = (float)w / (float)tex.width;
				Color color = gradient.Evaluate(rate);
				tex.SetPixel(w, TextureHight, color);
			}
			tex.Apply();
		}

		Texture2D CreateTemporaryTexture() {
			Texture2D texture = new Texture2D(TextureWidth, TextureHight, TextureFormat.RGB565, false);
			// texture.wrapMode = TextureWrapMode.Clamp;
			return texture;
		}

		string GetGradientDataDirectory () {
			string directoryPath = Path.GetDirectoryName(AssetDatabase.GetAssetPath(material));
			return Path.Combine(directoryPath, "Gradient");
		}

		string GetGradientDataPath() {
			string fileName = material.name + ".asset";
			return Path.Combine(GetGradientDataDirectory(), fileName);
		}

		string GetGradientTexturePath() {
			string fileName = material.name + ".png";
			return Path.Combine(GetGradientDataDirectory(), fileName);        
		}
	}
}