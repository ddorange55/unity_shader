﻿Shader "Custom/Color/ColorMap"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_ColorMap ("Texture", 2D) = "black" {}
		_MapValue("Map value", Range(-1,1)) = 0.0
	}
	SubShader
	{
		Tags
		{
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
		}
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode Off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile_particles
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _ColorMap;
			float4 _ColorMap_ST;
			float _MapValue;


			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 uv     : TEXCOORD0;
				float2 uv_col : TEXCOORD1;
				fixed4 color  : COLOR;
			};
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				o.uv_col = TRANSFORM_TEX(v.texcoord, _ColorMap);
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 main = tex2D(_MainTex, i.uv);
				
				// メインテクスチャの輝度を取得
				fixed lum = Luminance(main.rgb);

				// 輝度をUVのX軸座標として取得する
				fixed4 col = tex2D(_ColorMap, fixed2(lum, 0.0));

				return fixed4(col.rgb, main.r) * i.color;
			}
			ENDCG
		}
	}
}
