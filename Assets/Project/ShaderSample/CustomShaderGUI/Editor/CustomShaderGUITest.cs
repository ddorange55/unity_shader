﻿using UnityEngine;
using UnityEditor;
using System;

public class CustomShaderGUITest : ShaderGUI
{
    private static class Styles
    {
        public static GUIContent albedoText = new GUIContent("Albedo", "Albedo (RGB) and Transparency (A)");
    }

    MaterialProperty subTex = null;
    MaterialProperty tintColor = null;

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
    {
        // デフォルトの GUI をレンダリング
        base.OnGUI(materialEditor, properties);

        Material targetMat = materialEditor.target as Material;

        // isSubTex が設定されているかを確認してからチェックボックスを表示
        bool isSubTex = Array.IndexOf(targetMat.shaderKeywords, "SUB_TEX_ON") != -1;
        bool isTintColor = Array.IndexOf(targetMat.shaderKeywords, "TINT_COLOR_ON") != -1;
        
        subTex = FindProperty("_SubTex", properties);
        tintColor = FindProperty("_TintColor", properties);

        EditorGUI.BeginChangeCheck();

        // チェックボックスのサンプル
        EditorGUILayout.BeginVertical(GUI.skin.box);
        {
            isSubTex = EditorGUILayout.Toggle("Sub Tex", isSubTex);

            if (isSubTex) {
                // テクスチャ（通常）
                materialEditor.TextureProperty(subTex, "Sub Tex");
                
                // テクスチャ（シングルライン）
                // materialEditor.TexturePropertySingleLine(Styles.albedoText, subTex, tintColor);
            }
        }
        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical(GUI.skin.box);
        {
            isTintColor = EditorGUILayout.Toggle("TInt Color", isTintColor);

            if (isTintColor) {
                materialEditor.ColorProperty(tintColor, "Tint Color");
            }
        }
        EditorGUILayout.EndVertical();

        
        
        if (EditorGUI.EndChangeCheck())
        {
            // チェックボックスに基づくキーワードを有効化/無効化
            if (isSubTex)
                targetMat.EnableKeyword("SUB_TEX_ON");
            else
                targetMat.DisableKeyword("SUB_TEX_ON");

            if (isTintColor)
                targetMat.EnableKeyword("TINT_COLOR_ON");
            else
                targetMat.DisableKeyword("TINT_COLOR_ON");
        }
    }
}