﻿Shader "Effect/Basic" {

	Properties {
		
		_MainTex("Main Tex", 2D) = "white" {}

		// 頂点カラー
		_TintColor ("Tint Color", Color) = (0.5,0.5,0.5,1.0)

		// ブレンドモード
		[Enum(UnityEngine.Rendering.BlendOp)] _BlendOpMode("BlendOp Mode", Float) =	0
		_BlendMode ("Blend Mode", Float) = 0.0 // ブレンドモードキャッシュ
		[Enum(UnityEngine.Rendering.BlendMode)] _SrcBlendMode("Src Blend Mode", Float) = 5
		[Enum(UnityEngine.Rendering.BlendMode)] _DstBlendMode("Dst Blend Mode", Float) = 10

		// デプス制御
		[Enum(UnityEngine.Rendering.CullMode)] _CullMode("Culling", Float) = 2
		[KeywordEnum(OFF, ON)] _ZWriteMode("Z Write", Float) = 0
		[Enum(UnityEngine.Rendering.CompareFunction)] _DepthTestMode("Depth Test Mode", Float) = 8

		// カラー制御
		_ColorShiftValue ("Lerp Value", Range(-1, 1)) = 0
		_GrayScaleAlpha ("Alpha Boost", Range(0, 1)) = 0 // 0ならアルファ地をグレースケールとして計算する

		// Lerpカラー
		_LerpColorWhite ("Color From White", Color) = (1.0,1.0,1.0,1.0)
		_LerpColorBlack ("Color From Black", Color) = (0.0,0.0,0.0,0.0)

		// カラーマップ
		[KeywordEnum(OFF, LUM, X, Y, POLAR)] COLOR_MAP_MODE("Color Map Mode", Float) = 0
		_ColorMapTex("Color Map Tex", 2D) = "white" {}

		// マスク
		_MaskTex("Mask Tex(R)", 2D) = "white" {}

		// UVスクロール
		_ScrollSpeedX("UV Scroll Speed X", Float) = 0
		_ScrollSpeedY("UV Scroll Speed Y", Float) = 0

		// テクスチャの歪み
		_DistortionTex("Distortion Tex (GB)", 2D) = "white" {}
		_DistortionValue("Distortion Value", Float) = 0

		// カットアウト
		_DissolveTex   ("Dissolve Tex(R)", 2D) = "white" {}
		_DissolveValue ("Dissolve Value", Range(0, 1)) = 0
	}

	Category {
		Tags {
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}

		BlendOp [_BlendOpMode]
		Blend[_SrcBlendMode][_DstBlendMode]

		Lighting Off
		Cull [_CullMode]
		ZWrite [_ZWriteMode]
		ZTest [_DepthTestMode]

		SubShader {
			Pass {
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				
				// SoftParticle
				// #pragma multi_compile_particles
				
				// fogの設定
				// #pragma multi_compile_fog
				// // 不要なfogシェーダーバリアントを生成しないように調整
				// #pragma skip_variants FOG_LINEAR FOG_EXP2
				
				#pragma shader_feature LERP_COLOR_ON
				#pragma shader_feature COLOR_MAP_MODE_OFF COLOR_MAP_MODE_LUM COLOR_MAP_MODE_X  COLOR_MAP_MODE_Y  COLOR_MAP_MODE_POLAR
				#pragma shader_feature MASK_ON
				#pragma shader_feature UV_SCROLL_ON
				#pragma shader_feature DISTORTION_ON
				#pragma shader_feature CUT_OUT_ON

				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _MainTex_ST;
				float4 _TintColor;
				float _GrayScaleAlpha;
				float _ColorShiftValue;
			
			#ifdef LERP_COLOR_ON
				float4 _LerpColorWhite;
				float4 _LerpColorBlack;
			#endif

				// ColorMap
				sampler2D _ColorMapTex;
				float4 _ColorMapTex_ST;

			#ifdef MASK_ON
				sampler2D _MaskTex;
				float4 _MaskTex_ST;
				float _MaskValue;
			#endif

			#ifdef UV_SCROLL_ON
				uniform half _ScrollSpeedX;
				uniform half _ScrollSpeedY;
			#endif

			#ifdef DISTORTION_ON
				sampler2D _DistortionTex;
				float4 _DistortionTex_ST;
				float _DistortionValue;
			#endif

			#ifdef CUT_OUT_ON
				sampler2D _DissolveTex;
				float4 _DissolveTex_ST;
				float _DissolveValue;
			#endif

				struct appdata_t {
					float4 vertex    : POSITION;
					float2 texcoord  : TEXCOORD0;
					float4 color     : COLOR;
				};

				struct v2f {
					float4 pos     : POSITION;
					float2 uv      : TEXCOORD0;
					float2 uv_mask : TEXCOORD2;
					float2 uv_dist : TEXCOORD3;
					float2 uv_dssl : TEXCOORD4;
					float4 color   : COLOR;
					// UNITY_FOG_COORDS(3)
				};


				v2f vert (appdata_t v)
				{
					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.uv  = TRANSFORM_TEX (v.texcoord, _MainTex);
					o.color = v.color;

				#ifdef MASK_ON
					o.uv_mask = TRANSFORM_TEX(v.texcoord, _MaskTex);
				#endif

				#ifdef UV_SCROLL_ON
					o.uv += half2(_ScrollSpeedX, _ScrollSpeedY) * _Time;
				#endif

				#ifdef DISTORTION_ON
					o.uv_dist = TRANSFORM_TEX(v.texcoord, _DistortionTex);
				#endif

				#ifdef CUT_OUT_ON
					o.uv_dssl = TRANSFORM_TEX(v.texcoord, _DissolveTex);
				#endif
					// UNITY_TRANSFER_FOG(o,o.pos);
					return o;
				}

				fixed4 frag( v2f i ) : COLOR
				{
				
				#ifdef DISTORTION_ON
					fixed2 dist = tex2D(_DistortionTex, i.uv_dist).g * _DistortionValue;
					i.uv += dist;
				#endif
					fixed4 col = tex2D(_MainTex, i.uv);

					// グレースケールをアルファ値に変換する
					col.a *= col.r + _GrayScaleAlpha;
					// col.a *= pow(col.r, 2) + _GrayScaleAlpha; // テクスチャの出来が悪いと端までR値が残っていることがあるので累乗するのもあり

				#ifdef LERP_COLOR_ON
					col.rgb = lerp(_LerpColorBlack.rgb, _LerpColorWhite.rgb, saturate(col.r + _ColorShiftValue));
				#endif
				
				#ifdef COLOR_MAP_MODE_LUM
					// 輝度
					fixed lum = saturate(Luminance(col.rgb) + _ColorShiftValue);
					col.rgb = tex2D(_ColorMapTex, fixed2(lum, 0.0)).rgb;
				#elif COLOR_MAP_MODE_X
					// 横（X軸）
					fixed x = saturate(i.uv.x + _ColorShiftValue);
					col.rgb = tex2D(_ColorMapTex, fixed2(x, 0.0)).rgb;
				#elif COLOR_MAP_MODE_Y
					// 縦（Y軸）
					fixed y = saturate(i.uv.y + _ColorShiftValue);
					col.rgb = tex2D(_ColorMapTex, fixed2(y, 0.0)).rgb;
				#elif COLOR_MAP_MODE_POLAR
					// 円形s
					fixed len = distance(fixed2(0.5, 0.5), i.uv);
					col.rgb = tex2D(_ColorMapTex, fixed2( saturate(len + _ColorShiftValue), 0.0 )).rgb;
				#endif
				
				// マスク
				#ifdef MASK_ON
					fixed4 mask = tex2D(_MaskTex, i.uv_mask);
					col.a *= mask.r;
				#endif

				// ディゾルブ
				#ifdef CUT_OUT_ON
					fixed dssl = tex2D(_DissolveTex, i.uv_dssl).b;
					clip(dssl - _DissolveValue);
				#endif

					// 頂点カラーの設定
					col.rgb += (_TintColor.rgb - float3(0.5, 0.5, 0.5)) * 2;
					col.a *= _TintColor.a;

					col *= i.color;

					// UNITY_APPLY_FOG(i.fogCoord, col);
					return col;
				}
				ENDCG
			}
		}
	}
	CustomEditor "CustomShaderGUI.GeneralBasicShaderGUI"
}