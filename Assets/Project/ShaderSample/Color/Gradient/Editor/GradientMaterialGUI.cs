﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using CustomShaderGUI;

// MaterialEditorを継承するとCustomEditorの指定でnamespaceが効かなくなるため
// namespace CustomShaderGUI {
	public class GradientMaterialGUI : MaterialEditor {
		private Material material {
			get { return (Material)target; }
		}
		private GradientAssetData data;
		SerializedObject serializedGradientData;
		SerializedProperty gradientProperty;
		Texture2D temporaryTexture;
		const int TextureWidth = 256;
		const int TextureHight = 1;
    	bool isDirty; // 変更があったがSaveしていないのを検知する 

		public override void OnEnable() {
			// アセットデータを取得
			if (data == null) {
				string path = GetGradientDataPath();
				
				// Dataをロードする 
				data = AssetDatabase.LoadAssetAtPath<GradientAssetData>(path);
				
				if (temporaryTexture == null) {
					temporaryTexture = CreateTemporaryTexture();
				}

				// dataが存在しなければ作成する 
				if (data == null) {
					CreateAssetData(path);
				}

				serializedGradientData = new SerializedObject(data);
				gradientProperty = serializedGradientData.FindProperty("gradient");
				Gradient2Texture(data, ref temporaryTexture);
			}

			base.OnEnable();
		}

		void CreateAssetData (string path) {
			data = CreateInstance<GradientAssetData>();
			data.gradient = new Gradient();

			string directoryPath = GetGradientDataDirectory();
			if (!Directory.Exists(directoryPath)) {
				Directory.CreateDirectory(directoryPath);
			}
			
			// テクスチャを作る 
			string texturePath = GetGradientTexturePath();
			File.WriteAllBytes(texturePath, temporaryTexture.EncodeToPNG());
			AssetDatabase.Refresh();

			// textureの設定を変更する 
			TextureImporter texImporter = AssetImporter.GetAtPath(texturePath) as TextureImporter;
			if (texImporter != null) {
				texImporter.wrapMode = TextureWrapMode.Clamp;
				texImporter.isReadable = false;
				texImporter.mipmapEnabled = false;
			}

			// textureをセットする 
			var texture = AssetDatabase.LoadAssetAtPath<Texture2D>(texturePath);
			data.texture = texture;
			AssetDatabase.CreateAsset(data, path);
			SaveAssetData();
		}

		void SaveAssetData () {
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		public override void OnDisable() {
			// Debug.Log("OnDisable");
			material.SetTexture("_GradientTex", data.texture);
			
			if (isDirty) {
				if (EditorUtility.DisplayDialog(
					"警告",
					"テクスチャが変更されたがSaveされてません。セーブしますか？",
					"OK",
					"CANCEL")) {
					SaveTexture();
					isDirty = false;
				}
			}

			base.OnDisable();
		}

		public override void OnInspectorGUI() {
			if (isVisible == false) {
				return;
			}

			Material[] mats = { material };
			
			// メインテクスチャ設定
			var mainTex = GetMaterialProperty(mats, "_MainTex");
			TextureProperty(mainTex, "Main Tex (RGBA)");

			var gradTex = GetMaterialProperty(mats, "_GradientTex");
			TextureProperty(gradTex, "Gradient Tex (RGB)");

			// グラデーション設定
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(gradientProperty);
			if (EditorGUI.EndChangeCheck())
			{
				serializedGradientData.ApplyModifiedProperties();
				Gradient2Texture(data, ref temporaryTexture);
				material.SetTexture("_GradientTex", temporaryTexture);
				isDirty = true;
			}

			if (GUILayout.Button("Save"))
			{
				SaveTexture();
				isDirty = false;
			}
		}

		void SaveTexture() {
			File.WriteAllBytes(GetGradientTexturePath(), temporaryTexture.EncodeToPNG());
			AssetDatabase.Refresh();
		}

		void Gradient2Texture(GradientAssetData data, ref Texture2D texture) {
			if (texture == null) {
				Debug.Assert(false, "[GradientData] Texture is missing!!!");
				texture = CreateTemporaryTexture();
			}

			Gradient gradient = data.gradient;
			for (int w = 0; w < texture.width; ++w) {
				float rate = (float)w / (float)texture.width;
				Color color = gradient.Evaluate(rate);
				texture.SetPixel(w, TextureHight, color);
			}
			texture.Apply();
		}

		Texture2D CreateTemporaryTexture() {
			Texture2D texture = new Texture2D(TextureWidth, TextureHight, TextureFormat.RGB565, false);
			texture.wrapMode = TextureWrapMode.Clamp;
			return texture;
		}

		string GetGradientDataDirectory() {
			string directoryPath = Path.GetDirectoryName(AssetDatabase.GetAssetPath(material));
			return Path.Combine(directoryPath, "Gradient");
		}

		string GetGradientDataPath() {
			string fileName = material.name + ".asset";
			return Path.Combine(GetGradientDataDirectory(), fileName);
		}

		string GetGradientTexturePath() {
			string fileName = material.name + ".png";
			return Path.Combine(GetGradientDataDirectory(), fileName);        
		}
	}
// }