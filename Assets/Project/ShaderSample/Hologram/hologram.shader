﻿Shader "Custom/Hologram" {

	Properties {
		_MainTex("Main Tex", 2D) = "black" {}
		_PtnTex("Hologram Pattren Tex", 2D) = "white" {}
		_GlowTex("Glow Tex", 2D) = "black" {}
		_Rotate("Rotate", Range(0, 1)) = 0
	}

	Category {
		Tags {
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}
		
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off

		SubShader {
			Pass {
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _MainTex_ST;
				sampler2D _PtnTex;
				float4 _PtnTex_ST;
				sampler2D _GlowTex;
				float4  _GlowTex_ST;
				float _Rotate;

				struct appdata_t {
					float4 vertex : POSITION;
					float2 texcoord: TEXCOORD0;
				};

				struct v2f {
					float4 vertex  : POSITION;
					float2 uv      : TEXCOORD0;
					float2 uv_ptn  : TEXCOORD1;
					float2 uv_glow : TEXCOORD2;
				};

				v2f vert (appdata_t v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv  = TRANSFORM_TEX (v.texcoord, _MainTex);
					o.uv_ptn = TRANSFORM_TEX (v.texcoord, _PtnTex);
					return o;
				}

				fixed4 frag( v2f i ) : COLOR
				{
					fixed4 ptn = tex2D(_PtnTex, i.uv_ptn);
					fixed y = ptn.r + _Rotate;

					fixed4 bg = tex2D(_GlowTex, float2(_Rotate, y));
					
					fixed4 col = tex2D(_MainTex, i.uv);
					col.rgb *= col.a;
					col.rgb += bg.rgb * (1.0 - col.a);
					col.a =  1;

					return col;
				}
				ENDCG
			}
		}
	}
}