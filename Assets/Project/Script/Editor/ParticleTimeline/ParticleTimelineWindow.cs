﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;


namespace ParticleTimeline {
	public class ParticleTimelineWindow : EditorWindow {

		private const string WINDOW_NAME = "Particle Timeline";

		private ParticleModel particleModel;

		private TimelineView timelineView;

		private TimeControl timer;

		[MenuItem("Tool/" + WINDOW_NAME)]
		static void Open() {
			EditorWindow.GetWindow<ParticleTimelineWindow>(WINDOW_NAME);
		}
		
		void OnEnable() {
			
			// タイマーを初期化
			timer = new TimeControl();
			timer.SetMinMaxTime(0.0f, 1.0f);

			// モデルの初期化
			particleModel = new ParticleModel();

			// Viewの初期化
			timelineView = new TimelineView();
		}

		void OnDisable() {
			particleModel.Clear();
		}

		void Update() {
			SimulateParticle(timer.currentTime);

			if (timer.isPlaying) {
				Repaint();
			}
		}

		/// <summary>
		/// パーティクルをシュミレーションする
		/// </summary>
		private void SimulateParticle (float time) {
			
			// 再生リストが空の場合はスキップ
			if (particleModel.playList == null || particleModel.playList.Count <= 0) return;

			foreach (var item in particleModel.playList) {
				if (item.active) {
					var t = time - item.delay;
					if (t > 0) {
						item.particle.Simulate(t, false);
					}
				} else {
					item.particle.Clear();
				}
			}
		}

		void OnGUI() {

			GUILayout.Space(10);
			EditorGUILayout.BeginHorizontal();
			{
				// 現在の秒数
				var currentTime = (Mathf.Floor(timer.currentTime * 100) /100 );
				EditorGUILayout.LabelField("再生時間: " + currentTime.ToString(), GUILayout.Width(100), GUILayout.Height(24) );

				// 停止ボタン
				if( GUILayout.Button("停止", GUILayout.MaxWidth(80), GUILayout.Height(18)) ) {
					timer.Stop();
				}

				// 再生 / 一時停止ボタン
				var btnTxt = timer.isPlaying ? "一時停止" : "再生";
				if( GUILayout.Button(btnTxt, GUILayout.MaxWidth(80), GUILayout.Height(18)) ) {
					if (timer.isPlaying) {
						timer.Pause();
					} else {
						timer.Play();
					}
				}
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(20);
			timelineView.Create(timer, particleModel.playList);

			GUILayout.Space(10);
			EditorGUILayout.BeginHorizontal();
			{
				if ( GUILayout.Button("登録", GUILayout.MaxWidth(60), GUILayout.Height(18)) ) {
					RegisterPLayList();
				}
				if ( GUILayout.Button("クリア", GUILayout.MaxWidth(60), GUILayout.Height(18)) ) {
					timer.Stop();
					particleModel.Clear();
				}
			}
			EditorGUILayout.EndHorizontal();

			// キーボード対応
			OnDownArrowKey();
		}

		/// <summary>
		/// 再生リストにパーティクルを登録する
		/// </summary>
		private void RegisterPLayList () {
			timer.Stop();
			particleModel.Register(Selection.gameObjects);
			timer.SetMinMaxTime(0.0f, particleModel.maxDuration);

			// UIを描画し直す
			GUI.changed = true;
    		Event.current.Use();
			Repaint();
		}

		/// <summary>
		/// 矢印キーで再生を制御する
		/// </summary>
		private void OnDownArrowKey () {
			if (Event.current.type == EventType.KeyDown) {

				//再生中であれば一時停止させる
				timer.Pause ();

				if (Event.current.keyCode == KeyCode.RightArrow)
					if (timer.currentTime < timer.maxTime - 0.01f) {
						timer.currentTime += 0.01f;
					} else {
						timer.currentTime = timer.maxTime;
					}
					

				if (Event.current.keyCode == KeyCode.LeftArrow)
					if (timer.currentTime > 0.01f) {
						timer.currentTime -= 0.01f;
					} else {
						timer.currentTime = 0.0f;
					}
					
				// UIを描画し直す
				GUI.changed = true;
				Event.current.Use ();
				Repaint ();
			}
		}
	}
}