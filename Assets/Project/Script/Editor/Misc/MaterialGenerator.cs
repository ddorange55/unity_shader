﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


/// <summary>
/// 任意のディレクトリの中に含まれるテクスチャを設定したマテリアルを生成する
/// </summary>
public class MaterialGenerator : EditorWindow{
	/// <summary>
	/// テクスチャの格納されているディレクトリのパス
	/// </summary>
	string texDir;

	/// <summary>
	/// 生成するマテリアルを格納するディレクトリのパス
	/// </summary>
	string matDir;

	[MenuItem("Tool/Misc/Genarate Material")]
	public static void Open() {
		EditorWindow.GetWindow<MaterialGenerator>("Material Generator");
	}

	void OnGUI() {
		GUILayout.Space(5);

		EditorGUILayout.LabelField("Assetsディレクトリからのパスを入力してください");
		EditorGUILayout.LabelField("例）Assets/Project/Texture");

		GUILayout.Space(10);

		// テクスチャのパス
		texDir = EditorGUILayout.TextField("Texture Dir Path", texDir);
		
		// マテリアルのパス
		matDir = EditorGUILayout.TextField("Material Dir Path", matDir);

		GUILayout.Space(10);
		
		// Loadボタン
		if( GUILayout.Button("Create", GUILayout.MaxWidth(120), GUILayout.Height(18)) ) {
			CreateMaterial(texDir, matDir);
		}
	}

	/// <summary>
	/// テクスチャからマテリアルを生成する
	/// </summary>
	private static void CreateMaterial (string texPath, string matPath) {
	
		string[] files = System.IO.Directory.GetFiles(texPath, "*.png", SearchOption.AllDirectories);
		
		try
		{
			AssetDatabase.StartAssetEditing();

			foreach (var file in files) {
				Texture tex = AssetDatabase.LoadAssetAtPath<Texture>(file);
				Debug.Log(file);
				Debug.Log(tex.name);
				Material mat = new Material(Shader.Find("Mobile/Particles/Additive"));
				mat.mainTexture = tex;
				AssetDatabase.CreateAsset(mat, Path.Combine(matPath, tex.name + ".mat"));
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}
		}
		catch (Exception e)
		{
			Debug.LogError(e);
		}
		finally
		{
			AssetDatabase.StopAssetEditing();
		}
	}
}