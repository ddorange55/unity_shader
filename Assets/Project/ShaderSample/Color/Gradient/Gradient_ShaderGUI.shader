﻿Shader "Custom/Color/Gradient(ShaderGUI)"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_GradientTex("Gradient Tex", 2D) = "white" {}
		_GradientAssetId("Gradient Asset Id", Int) = 0
	}

	SubShader
	{
		Tags {
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
		}
		LOD 100
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		ZTest LEqual

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			sampler2D _GradientTex;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 gradient = tex2D(_GradientTex, float2(col.r, 0));
				return fixed4(gradient.rgb, col.a);
			}
			ENDCG
		}
	}
	CustomEditor "CustomShaderGUI.GradientShaderGUI"
}
