﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace CustomShaderGUI {
    public class GeneralBasicShaderGUI : GeneralShaderGUIBase
    {
        protected enum COLOR_MAP_MODE {
            OFF = 0,
            LUMINANCE,
            HORIZONTAL,
            VERTICAL,
            POLAR
        }
        protected MaterialProperty lerpColorWhite = null;
        protected MaterialProperty lerpColorBlack = null;
        protected MaterialProperty colorShiftVal = null;
        protected MaterialProperty grayScaleAlpha = null;
        protected MaterialProperty colorMapTex = null;
        protected MaterialProperty colorMapMode = null;
        protected MaterialProperty maskTex = null;
        protected MaterialProperty uvScrollX = null;
        protected MaterialProperty uvScrollY = null;
        protected MaterialProperty distTex = null;
        protected MaterialProperty distVal = null;
        protected MaterialProperty dsslTex = null;
        protected MaterialProperty dsslVal = null;
        bool enableGrayScaleAplha;
        bool enableColorMapMode;
        bool GetColorMapModeEnable (Material mat) {
            return (COLOR_MAP_MODE)mat.GetFloat("COLOR_MAP_MODE") != COLOR_MAP_MODE.OFF;
        }

        /// <summary>
        /// Shaderに設定されているプロパティを取得する
        /// </summary>
        public override void FindProperties(MaterialProperty[] props) {
            base.FindProperties(props);
            lerpColorWhite = FindProperty("_LerpColorWhite", props);
            lerpColorBlack = FindProperty("_LerpColorBlack", props);
            colorShiftVal  = FindProperty("_ColorShiftValue",props);
            grayScaleAlpha = FindProperty("_GrayScaleAlpha", props);
            colorMapTex    = FindProperty("_ColorMapTex",    props);
            colorMapMode   = FindProperty("COLOR_MAP_MODE",  props); 
            maskTex        = FindProperty("_MaskTex",        props);
            uvScrollX      = FindProperty("_ScrollSpeedX",   props);
            uvScrollY      = FindProperty("_ScrollSpeedY",   props);
            distTex        = FindProperty("_DistortionTex",  props);
            distVal        = FindProperty("_DistortionValue",props);
            dsslTex        = FindProperty("_DissolveTex",    props);
            dsslVal        = FindProperty("_DissolveValue",  props);
        }

        /// <summary>
		/// ShaderのプロパティをGUIに表示する
		/// </summary>
        public override void ShaderPropertiesGUI(MaterialEditor editor, Material mat) {
            EditorGUIUtility.labelWidth = 0f;
            
            MainSettingArea(mat, editor);
            LerpColorArea(mat, editor);     // Lerpカラー
            // PreBlendModeArea(mat, editor);  // 事前ブレンド
            UVScrollArea(mat, editor);      // UVスクロール
            MaskArea(mat, editor);          // マスク
            DistArea(mat, editor);
            DissolveArea(mat, editor);
            editor.RenderQueueField();
        }

        /// <summary>
        /// LerpColor機能のUI
        /// </summary>
        protected void LerpColorArea (Material mat, MaterialEditor editor) {
            bool enableLerpColor = Array.IndexOf(mat.shaderKeywords, "LERP_COLOR_ON") != -1;
            enableColorMapMode = GetColorMapModeEnable(mat);
            
            EditorGUILayout.BeginVertical(GUI.skin.box);
            {
                EditorGUILayout.LabelField("Color Control", EditorStyles.boldLabel);
                
                // グレースケールをアルファに変換する機能のON/OFF
                if (grayScaleAlpha != null) {
                    enableGrayScaleAplha = grayScaleAlpha.floatValue == 0.0f;
                    EditorGUI.BeginChangeCheck();
                    enableGrayScaleAplha = EditorGUILayout.Toggle("Gray Scale Alpha", enableGrayScaleAplha);
                    if (EditorGUI.EndChangeCheck()) {
                        mat.SetFloat("_GrayScaleAlpha", enableGrayScaleAplha? 0.0f : 1.0f);
                        EditorUtility.SetDirty(mat);
                    }
                }

                // カラーマッピングモード
                EditorGUI.BeginChangeCheck();
                editor.ShaderProperty(colorMapMode, "Color Map Mode");
                if (EditorGUI.EndChangeCheck()) {
                    enableColorMapMode = GetColorMapModeEnable(mat);
                }

                if (enableColorMapMode) {
                    // カラーマップテクスチャ
                    editor.TextureProperty(colorMapTex, "Color Map Tex(RGB)");

                    // カラーシフト機能
                    editor.RangeProperty(colorShiftVal, "Color Shift");
                }

                // LerpColor機能
                // EditorGUI.BeginChangeCheck();
                // enableLerpColor = EditorGUILayout.Toggle("Lerp Color", enableLerpColor);
                // if (EditorGUI.EndChangeCheck()) {
                //     SetKeyword(mat, "LERP_COLOR_ON", enableLerpColor);
                // }
                // if (enableLerpColor) {
                //     editor.ColorProperty(lerpColorWhite, "Color From White");
                //     editor.ColorProperty(lerpColorBlack, "Color From Black");
                // }
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// マスク機能のUI
        /// </summary>
        protected void MaskArea (Material mat, MaterialEditor editor) {
            bool enableMask = Array.IndexOf(mat.shaderKeywords, "MASK_ON") != -1;

            EditorGUILayout.BeginVertical(GUI.skin.box);
            {   
                EditorGUI.BeginChangeCheck();
                enableMask = EditorGUILayout.ToggleLeft("Mask", enableMask, EditorStyles.boldLabel);
                if (EditorGUI.EndChangeCheck()) {
                    SetKeyword(mat, "MASK_ON", enableMask);
                }

                if (enableMask) {
                    editor.TextureProperty(maskTex, "Mask Tex (R)");
                }
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// UVスクロール
        /// </summary>
        protected void UVScrollArea (Material mat, MaterialEditor editor) {
            bool enableUVScroll = Array.IndexOf(mat.shaderKeywords, "UV_SCROLL_ON") != -1;

            EditorGUILayout.BeginVertical(GUI.skin.box);
            {
                EditorGUI.BeginChangeCheck();
                enableUVScroll = EditorGUILayout.ToggleLeft("UVScroll", enableUVScroll, EditorStyles.boldLabel);
                if (EditorGUI.EndChangeCheck()) {
                    SetKeyword(mat, "UV_SCROLL_ON",  enableUVScroll);
                }

                if (enableUVScroll) {
                    editor.FloatProperty(uvScrollX, "Scroll X");
                    editor.FloatProperty(uvScrollY, "Scroll Y");
                }
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// テクスチャのゆがみ設定
        /// </summary>
        protected void DistArea (Material mat, MaterialEditor editor) {
            bool enable = Array.IndexOf(mat.shaderKeywords, "DISTORTION_ON") != -1;
            
            EditorGUILayout.BeginVertical(GUI.skin.box);
            {
                EditorGUI.BeginChangeCheck();
                enable = EditorGUILayout.ToggleLeft("Distortion", enable, EditorStyles.boldLabel);
                if (EditorGUI.EndChangeCheck()) {
                    SetKeyword(mat, "DISTORTION_ON",  enable);
                }

                if (enable) {
                    editor.TextureProperty(distTex, "Texture (G)");
                    editor.FloatProperty(distVal, "Strength");
                }
            }
            EditorGUILayout.EndVertical();
        }

        protected void DissolveArea (Material mat, MaterialEditor editor) {
            ToggleBox(
                mat,
                editor,
                "Dissolve",
                "CUT_OUT_ON",
                () => {
                    editor.TextureProperty(dsslTex, "Texture (B)");
                    editor.RangeProperty(dsslVal, "Strength");
                });
        }

        protected void ToggleBox (Material mat, MaterialEditor editor, string title, string key, System.Action ui) {
            bool enable = Array.IndexOf(mat.shaderKeywords, key) != -1;

            EditorGUILayout.BeginVertical(GUI.skin.box);
            {
                EditorGUI.BeginChangeCheck();
                enable = EditorGUILayout.ToggleLeft(title, enable, EditorStyles.boldLabel);
                if (EditorGUI.EndChangeCheck()) {
                    SetKeyword(mat, key, enable);
                }

                if (enable && ui != null) {
                    ui();
                }
            }
            EditorGUILayout.EndVertical();
        }
    }
}