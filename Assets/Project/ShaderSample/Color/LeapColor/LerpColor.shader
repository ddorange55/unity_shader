﻿Shader "Custom/Color/LerpColor"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_LerpColorWhite ("Color White", Color) = (1.0,1.0,1.0,1.0)
		_LerpColorBlack ("Color Black", Color) = (0.0,0.0,0.0,0.0)
	}
	SubShader
	{
		Tags
		{
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
		}
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode Off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile_particles
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _LerpColorWhite;
			fixed4 _LerpColorBlack;

			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
			};
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed lum = Luminance(col.rgb);
				col.rgb = lerp(_LerpColorBlack.rgb, _LerpColorWhite.rgb, lum);
				col *= i.color;
				return col;
			}
			ENDCG
		}
	}
}
