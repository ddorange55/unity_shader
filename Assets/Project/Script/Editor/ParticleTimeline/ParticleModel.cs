﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace ParticleTimeline {
	public class ParticleModel {

		/// <summary>
		/// 再生リストに登録されているパーティクルの最長Duration
		/// </summary>
		public float maxDuration { get; private set; }

		/// <summary>
		/// 再生リストの要素
		/// </summary>
		public class PlayListItem {
			public string name;
			public float dur = 0.0f;
			public float delay = 0.0f;
			public bool active = true;
			public int depth = 0;
			public ParticleSystem particle;
		}

		/// <summary>
		/// 再生リスト
		/// </summary>
		public List<PlayListItem> playList { get; private set; }
		
		/// <summary>
		/// コンストラクタ
		/// </summary>
		public ParticleModel () {
			this.playList = new List<PlayListItem>();
		}
		
		/// <summary>
		/// 再生リストに登録する
		/// </summary>
		public void Register (GameObject[] list) {
			// リストをクリア
			this.Clear();

			// 中のParticleSystemを取得
			List<ParticleSystem> particles = new List<ParticleSystem>();
			foreach (var obj in list) {
				particles.AddRange( obj.GetComponentsInChildren<ParticleSystem>(true) );
			}

			// 再生リストに登録
			foreach (var particle in particles) {
				var so = new SerializedObject(particle);
				var delay = so.FindProperty("startDelay.scalar").floatValue;

				if ( particle != null ) {
					playList.Add( new PlayListItem {
						name     = particle.name,
						dur      = particle.main.duration,
						delay    = delay,
						particle = particle
					} );
				}

				// 一旦delayを0にする
				SetParticleSerializedProp(particle, "startDelay.scalar", 0.0f);
			}

			this.maxDuration = this.playList.Select( o => o.particle.main.duration ).Max();
		}

		/// <summary>
		/// 再生リストをクリアする
		/// </summary>
		public void Clear () {
			// 0にしたdelayの値を元に戻す
			foreach ( var item in playList ) {
				SetParticleSerializedProp(item.particle, "startDelay.scalar", item.delay);
			}

			this.playList.Clear();
		}

		private void SetParticleSerializedProp (ParticleSystem ps, string propName, float val) {
			var so = new SerializedObject(ps);
			so.FindProperty(propName).floatValue = val;
			so.ApplyModifiedProperties();
		}
	}
}
