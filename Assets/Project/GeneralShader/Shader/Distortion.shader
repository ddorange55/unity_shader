﻿Shader "Effect/Distortion" {

	Properties {
		_MainTex  ("Main Tex", 2D) = "white" {}
		_NoiseTex ("Noise Tex(RG)", 2D) = "white" {}		
		_Strength ("Disp Strength X", Float) = 1
		_ScrollX  ("Scroll X", Float) = 0
		_ScrollY  ("Scroll X", Float) = 0
	}

	Category {
		Tags {
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		Lighting Off
		ZWrite Off

		SubShader {
			GrabPass {}

			Pass {
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 3.0
				#include "UnityCG.cginc"

				struct appdata_t {
					float4 vertex : POSITION;
					fixed4 color : COLOR;
					float2 texcoord: TEXCOORD0;
				};

				struct v2f {
					float4 vertex : POSITION;
					fixed4 color  : COLOR;
					float4 uv_grab   : TEXCOORD0;
					float2 uv        : TEXCOORD1;
					float2 uv_noise : TEXCOORD3;
				};

				sampler2D _GrabTexture;
				sampler2D _MainTex;
				float4 _MainTex_ST;
				sampler2D _NoiseTex;
				float4 _NoiseTex_ST;
				half _ScrollX;
				half _ScrollY;
				half _Strength;

				v2f vert (appdata_t v)
				{	
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv_grab  = ComputeGrabScreenPos(o.vertex);
					o.uv       = TRANSFORM_TEX(v.texcoord, _MainTex);
					o.uv_noise = TRANSFORM_TEX(v.texcoord, _NoiseTex);
					o.uv_noise += half2(_ScrollX, _ScrollY) * _Time;
					o.color = v.color;
					return o;
				}

				half4 frag( v2f i ) : COLOR
				{
					fixed4 main = tex2D(_MainTex, i.uv);

					fixed4 noise = tex2D(_NoiseTex, i.uv_noise);
					i.uv_grab += noise * _Strength;
					fixed4 grab = tex2Dproj( _GrabTexture, i.uv_grab);
					
					grab.a = pow(Luminance(main), 4) * i.color.a;
					return grab;
				}
				ENDCG
			}
		}
	}
}