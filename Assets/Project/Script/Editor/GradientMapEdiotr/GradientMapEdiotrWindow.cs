﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEditor;
using EditorHelper;

namespace GradientMapEdiotr {
	public class GradientMapEdiotrWindow : EditorWindow {
		
	#region 定数
		const int GRADIENT_TEX_WIDTH = 256;
		const int GRADIENT_TEX_HIGHT = 1;
		const string GRADIENT_ASSET_DIR = "Assets/Project/Data/GradientMap";
		const string GRADIENT_DATA_DIR = GRADIENT_ASSET_DIR + "/asset";
		const string GRADIENT_TEX_DIR = GRADIENT_ASSET_DIR + "/tex";
		const string GRADIENT_PRESET_TEX_DIR = GRADIENT_ASSET_DIR + "/tex_preset";
		const int THUMBNAIL_TEX_WIDTH = 160;
		const int THUMBNAIL_TEX_HIGHT = 18;
	#endregion
		
	#region フィールド/プロパティ
		// アセットデータ情報
		string assetDataName;
		GradientAssetData assetData;
		SerializedObject serializedData;
		SerializedProperty gradientProperty;

		// テクスチャのキャッシュ
		List<Texture> customTexList = new List<Texture>();
		List<Texture> presetTexList = new List<Texture>();

		// レイアウト情報
		Vector2 customTexListScrollPos = Vector2.zero;
		Vector2 presetTexListScrollPos = Vector2.zero;
		string[] tabs = {"Custom", "Preset"};
		int selectedTab = 0;
	#endregion
    
    #region メソッド
		[MenuItem("Tool/Creative/GradientionMapEditor")]
        static void Open() {
            EditorWindow.GetWindow<GradientMapEdiotrWindow>("Gradiention Map Editor");
        }
		void OnEnable() {
			LoadExistingTex();
		}

		void OnGUI() {
			if (assetData == null) {
				StartView ();
			} else {
				EditView();
			}
		}

		/// <summary>
        /// データの読み込みと新規作成のUIを表示する
        /// </summary>
		void StartView () {
			selectedTab = EditorHelper.GUIUtility.Tab(tabs, selectedTab);

			if (selectedTab == 0) {
				// カスタムグラデーションの表示
				HorizontalThumbnailList(customTexList, customTexListScrollPos, (name) => {
					if ( GUILayout.Button(name, GUILayout.MaxWidth(80), GUILayout.Height(18)) ) {
						assetData = AssetDatabase.LoadAssetAtPath<GradientAssetData>( GradientAssetPath(name) );
						LoadAssetData(assetData);
					}
				});
			} else if (selectedTab == 1) {
				// プリセットグラデーションの表示
				HorizontalThumbnailList(presetTexList, presetTexListScrollPos, (name) => {
					EditorGUILayout.LabelField(name, GUILayout.Width(80));
				});
			}

			EditorHelper.GUIUtility.Rule();
		
			EditorGUILayout.BeginHorizontal();
			{
				assetDataName = EditorGUILayout.TextField("", assetDataName);
				if ( GUILayout.Button("Add", GUILayout.MaxWidth(120), GUILayout.Height(18)) ) {
					CreateAssetData(assetDataName);
				}
			}
			EditorGUILayout.EndHorizontal();
		}

		/// <summary>
        /// グラデーションの編集UIを表示する
        /// </summary>
		void EditView () {
			EditorGUILayout.Space();

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(gradientProperty, false, null);
			if (EditorGUI.EndChangeCheck())
			{
				serializedData.ApplyModifiedProperties();
				EditorApplication.delayCall += () =>{
					GenerateTexture(assetData, ref assetData.texture);
				};
			}

			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.FlexibleSpace();
				if ( GUILayout.Button("Save", GUILayout.MaxWidth(120), GUILayout.Height(18)) ) {
					Debug.Log("Save");
					SaveAssetData(assetData);
					LoadExistingTex();
					assetData = null;
				}
			}
			EditorGUILayout.EndHorizontal();
		}

		/// <summary>
        /// グラーデーションアセットを生成する
        /// </summary>
		void CreateAssetData (string name) {
			if (System.String.IsNullOrEmpty(name) ) {
				Debug.LogError("アセットデータの名前が空です");
				return;
			}

			if ( customTexList.Count(t => t.name == name) > 0 ) {
				Debug.LogError("既に" + name + "という名前のアセットデータが存在します");
				return;
			}

			assetData = CreateInstance<GradientAssetData>();
			assetData.gradient = new Gradient();

			// テクスチャ作成
			string texPath = GradientTexPath(name);

			var tex = new Texture2D(GRADIENT_TEX_WIDTH, GRADIENT_TEX_HIGHT, TextureFormat.RGB565, false);
			File.WriteAllBytes(texPath, tex.EncodeToPNG());
			AssetDatabase.Refresh();

			// textureの設定を変更する 
			TextureImporter texImporter = AssetImporter.GetAtPath(texPath) as TextureImporter;
			if (texImporter != null) {
				texImporter.wrapMode = TextureWrapMode.Clamp;
				texImporter.isReadable = true;
				texImporter.mipmapEnabled = false;
			}
			AssetDatabase.ImportAsset(texPath, ImportAssetOptions.ForceUpdate);
			assetData.texture = AssetDatabase.LoadAssetAtPath<Texture2D>(texPath);

			AssetDatabase.CreateAsset(assetData, GradientAssetPath(name));
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();

			LoadAssetData(assetData);
		}

		/// <summary>
        /// グラーデーションアセットを生成する
        /// </summary>
		void LoadAssetData (GradientAssetData data) {
			if (data == null) return;

			// シリアライズオブジェクトを作成
			serializedData = new SerializedObject(data);
			gradientProperty = serializedData.FindProperty("gradient");
		}

		void SaveAssetData(GradientAssetData data) {
			// テクスチャを保存
			File.WriteAllBytes(GradientTexPath(data.name), data.texture.EncodeToPNG());

			// アセットを保存
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		void GenerateTexture(GradientAssetData data, ref Texture2D texture) {
			if (texture == null) return;

			Gradient gradient = data.gradient;
			for (int w = 0; w < texture.width; ++w) {
				float rate = (float)w / (float)texture.width;
				Color color = gradient.Evaluate(rate);
				texture.SetPixel(w, GRADIENT_TEX_HIGHT, color);
			}
			texture.Apply();
		}

		string GradientTexPath (string name) {
			return Path.Combine(GRADIENT_TEX_DIR, name + ".png");
		}

		string GradientAssetPath (string name) {
			return Path.Combine(GRADIENT_DATA_DIR, name + ".asset");
		}

		/// <summary>
        /// 既存グラデーションテクスチャを読み込む
        /// </summary>
		void LoadExistingTex () {
			customTexList = GetGradientTex(GRADIENT_TEX_DIR);
			presetTexList = GetGradientTex(GRADIENT_PRESET_TEX_DIR);
		}

		List<Texture> GetGradientTex (string path) {
			string[] files = System.IO.Directory.GetFiles(path, "*.png", SearchOption.AllDirectories);
			List<Texture> list = new List<Texture>();
			
			foreach (var file in files) {
				var tex = AssetDatabase.LoadAssetAtPath<Texture>(file);
				list.Add(tex);
			}
			return list;
		}

		void HorizontalThumbnailList (List<Texture> list, Vector2 sPos, System.Action<string> label) {
			sPos = EditorGUILayout.BeginScrollView(sPos, GUILayout.ExpandWidth(true), GUILayout.MaxHeight(400));
			{
				foreach (var tex in list) {
					EditorGUILayout.Space();
					
					// ラベルを表示する
					label(tex.name);
					
					// テクスチャのサムネイルを表示する
					var pos = GUILayoutUtility.GetLastRect();
					pos.position += new Vector2(pos.size.x + 4, 0);
					pos.size = new Vector2(THUMBNAIL_TEX_WIDTH, THUMBNAIL_TEX_HIGHT);
					EditorGUI.DrawPreviewTexture(pos, tex);
				}
			}
			EditorGUILayout.EndScrollView();			
		}
	#endregion
	}
}