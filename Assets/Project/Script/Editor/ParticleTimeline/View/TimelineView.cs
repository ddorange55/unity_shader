﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace ParticleTimeline {
	public class TimelineView {

		public float leftColumWidth { get; set; }
		private Color[] colorPalette = {
			new Color( 0.3f,  0.3f,  0.3f  ),
			new Color( 0.35f, 0.35f, 0.35f )
		};

		/// <summary>
		/// 背景色変更用ヘルパー
		/// </summary>
		private Color bgColor;
		private void BeginBgColor (Color col) {
			bgColor = GUI.backgroundColor;
			GUI.backgroundColor = col;
		}
		private void EndBgColor () {
			GUI.backgroundColor = bgColor;
		}

		private Rect sliderRect;
		public TimelineView () {
			this.leftColumWidth = 200.0f;
		}

		public void Create ( TimeControl timer, List<ParticleModel.PlayListItem> playList  ) {
			
			// シークバーを表示する
			CreateSeekBar(timer);

			// タイムラインを表示する
			CreateTimeline(playList);


			// 目盛りを表示する
			var timeLength = timer.maxTime - timer.minTime;						//時間の長さ
			var gridline = timeLength * 2;										//0.5目盛ごと
			var h = sliderRect.y + sliderRect.height + (playList.Count * 24); 	// メモリの高さ

			for (int i = 1; i < gridline; i++) {
				var x = (sliderRect.width / gridline) * i;
				x += 4f - 1.5f * (i - 1);
				Handles.color = new Color(0.5f, 0.5f, 0.5f);
				Handles.DrawLine ( new Vector2 (sliderRect.x + x, sliderRect.y), new Vector2 (sliderRect.x + x, h) );
				Handles.Label ( new Vector2 (sliderRect.x + x - 10, sliderRect.y - 10), (timeLength / gridline * i).ToString ("0.0") );
			}
		}

		/// <summary>
		/// シークバーを描画する
		/// </summary>
		private void CreateSeekBar (TimeControl timer) {

			EditorGUILayout.BeginHorizontal();
			{
				EditorGUILayout.BeginHorizontal( GUILayout.Width(leftColumWidth), GUILayout.Height(24) );
				{
					EditorGUILayout.LabelField("");
				}
				EditorGUILayout.EndHorizontal();

				timer.currentTime = GUILayout.HorizontalSlider( timer.currentTime, timer.minTime, timer.maxTime, "box", "Grad Down Swatch", GUILayout.ExpandWidth (true), GUILayout.Height(24) ); 
				sliderRect = GUILayoutUtility.GetLastRect();
			}
			EditorGUILayout.EndHorizontal();
		}

		/// <summary>
		/// タイムラインを描画する
		/// </summary>
		private void CreateTimeline (List<ParticleModel.PlayListItem> playList) {
			int i = 0;
			
			foreach ( var item in playList ) {
				
				var idx = i%2;

				EditorGUILayout.BeginHorizontal( GUILayout.ExpandWidth (true) );
				{	
					// 左カラム
					var sty = GetPlaneStyle();
					BeginBgColor( colorPalette[idx] );
					EditorGUILayout.BeginHorizontal( sty, GUILayout.Width(leftColumWidth), GUILayout.Height(24) );
					{
						item.active = EditorGUILayout.ToggleLeft(InsertDepthText(item.name, i), item.active);
					}
					EditorGUILayout.EndHorizontal();
					EndBgColor();

					// 右カラム
					BeginBgColor( colorPalette[idx] * 0.8f);
					EditorGUILayout.BeginHorizontal( sty, GUILayout.ExpandWidth (true), GUILayout.Height(24) );
					{
						EditorGUILayout.LabelField("");
					}
					EditorGUILayout.EndHorizontal();
					EndBgColor();
				}
				EditorGUILayout.EndHorizontal();

				i++;
			}
		}

		/// <summary>
		/// Track用のGUIスタイルを取得する
		/// </summary>
		private GUIStyle GetPlaneStyle () {
			GUIStyle sty = new GUIStyle();
			sty.margin = new RectOffset();
			sty.padding = new RectOffset();
			sty.alignment = TextAnchor.MiddleCenter;
			sty.normal.background = Texture2D.whiteTexture;
			return sty;
		}

		/// <summary>
		/// 階層構造を表示する為の文字を追加する
		/// </summary>
		private string InsertDepthText (string text, int depth) {
			var tree = "";

			for (var i = 0; i < depth; i++) {
				tree += (i == 0)? "└─" : "──";
			}

			return tree + " " + text;
		}
	}
}
