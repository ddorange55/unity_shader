﻿Shader "Custom/CustomEditorSample" {
    
	Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}

		[HideInInspector]
		_SubTex ("Sub (RGB)", 2D) = "white" {}

		[HideInInspector]
		_TintColor ("Tint Color", Color) = (1.0,1.0,1.0,1.0)
    }
    
	SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 200
        
        CGPROGRAM
        #pragma surface surf Lambert addshadow
        #pragma shader_feature SUB_TEX_ON
		#pragma shader_feature TINT_COLOR_ON

        sampler2D _MainTex;
		sampler2D _SubTex;
		float4 _SubTex_ST;
		float4 _TintColor;

        struct Input {
            float2 uv_MainTex;
        };

        void surf (Input IN, inout SurfaceOutput o) {
            half4 c = tex2D (_MainTex, IN.uv_MainTex);
            o.Albedo = c.rgb;
            o.Alpha = c.a;

            #if REDIFY_ON
            o.Albedo.gb *= 0.5;
            #endif
        }
        ENDCG
    } 
    CustomEditor "CustomShaderGUITest"
}